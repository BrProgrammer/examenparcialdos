package facci.pm.vera.denisse.examen;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;

public class Adapter_Recycler extends RecyclerView.Adapter<Adapter_Recycler.ViewH>{

    private ArrayList<Modelo_Recycler> modelo_recyclerArrayList;

    public Adapter_Recycler(ArrayList<Modelo_Recycler> modelo_recyclerArrayList) {
        this.modelo_recyclerArrayList = modelo_recyclerArrayList;
    }

    @NonNull
    @Override
    public ViewH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewH(LayoutInflater.from(parent.getContext()).inflate(R.layout.vista_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewH holder, int position) {

    }

    @Override
    public int getItemCount() {
        return modelo_recyclerArrayList.size();
    }

    public static class ViewH extends RecyclerView.ViewHolder{

        public ViewH(@NonNull View itemView) {
            super(itemView);
        }
    }
}
